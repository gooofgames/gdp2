﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SelectableFruitManager : MonoBehaviour {


	public int fruitPrice;
	public bool isBought;
	public string fruitState;
	public int lastCoinValue;
	public Sprite Fruit;
	public int minimumLevel; //annak a szintnek az értéke, amitől az adott fruit elérhető
	public DataManager dataManager;
	public Text myButtonText;
	public Text myFruitPriceText;
	//public bool allSlotsAreEmpty;
	public Button playButton;


	// Use this for initialization
	void Start () {
		dataManager= new DataManager();
		isBought = false;
		int diamonds=ApplicationData.diamonds;
		SetState (diamonds);
		PlayerPrefs.SetString ("Slot1", null);
		PlayerPrefs.SetString ("Slot2", null);
		PlayerPrefs.SetString ("Slot3", null);
		PlayerPrefs.SetString ("Slot4", null);
		PlayerPrefs.SetString ("Slot5", null);
		myFruitPriceText.text = ""+fruitPrice;
	//	allSlotsAreEmpty = true;

	}
	
	// Update is called once per frame
	void Update () {
		int diamonds = ApplicationData.diamonds;
		if (diamonds != lastCoinValue){
			SetState (diamonds);
		}
	/*	if (allSlotsAreEmpty) {
			playButton.interactable =false;
		} else {
			playButton.interactable=true;
		}*/
	}
	public void PressButton(){
		int slotNumber = 9;
		//Debug.Log ("Pressed");
		//keressük meg a következő üres slotot!

		GameObject[] slots = GameObject.FindGameObjectsWithTag ("Slot");

		//Debug.Log (slots.Length);
		foreach (GameObject slot in slots) {
			var slotManager = slot.GetComponent<SlotManager> ();
			string state = slotManager.slotState;
			//Debug.Log ("Name, state: " + slot.name + ", " + state);
			if (state == "Empty") {
				int slotNameLastChar = int.Parse(slot.name.Substring(slot.name.Length-1));

				if (slotNameLastChar < slotNumber) {
					slotNumber = slotNameLastChar;
				}
			
			}
		} 
		//Debug.Log (slotNumber);
		string nextEmptySlotname = string.Concat ("Slot", slotNumber);
		//Debug.Log (nextEmptySlotname);
		if (nextEmptySlotname != "Slot7"){ //ha van üres Slot
			GameObject toChangeGameObject = GameObject.Find (nextEmptySlotname);
			//GameObject toChangeGameObject = toChangeGameObjectParent.
			string nameOfPressedButton = this.name;
			toChangeGameObject.GetComponent<SpriteRenderer> ().sprite = Fruit;
			toChangeGameObject.GetComponent<Transform> ().transform.localScale = new Vector3(1,1,1);
			//Debug.Log (toChangeGameObjectSprite);
			Debug.Log (nameOfPressedButton);
			//Debug.Log (toChangeGameObjectSprite.name);
			isBought=true;
			toChangeGameObject.GetComponent<SlotManager> ().slotState = "NotEmpty";
			dataManager.UpdateDiamonds (ApplicationData.diamonds - fruitPrice);
			PlayerPrefs.SetString (toChangeGameObject.name, nameOfPressedButton);//elmentjük az adott slot tartalmát, ezt kell majd a game scene-ben megnyitni
		}

	}


	public void SetState(int diamonds){

		Button myButton=this.GetComponent<Button>();
		//Text myButtonText = myButton.GetComponentInChildren<Text> ();
		int maxLevel = ApplicationData.maxLevel;
		//Debug.Log ("Maxlevel: " + maxLevel);
		if (isBought == true) {
			fruitState = "Bought";
			myButton.interactable = false;
			myButtonText.text = "Chosen";

		} else {
			if (minimumLevel > maxLevel) {
				fruitState="Available soon";
				myButtonText.text ="Soon";
				myButton.interactable = false;
			} else {
				//int coins = AdManager.coins;
				lastCoinValue = diamonds;
				Debug.Log ("Diamonds= " + diamonds);
				if (diamonds >= fruitPrice) {
					fruitState = "Available";
					myButton.interactable = true;
					myButtonText.text = "Choose";

				} else {
					fruitState = "NotEnoughCoins";
					myButton.interactable = false;
					myButtonText.text = "Not Enough Coins";

				}
			}
		}
	}
}

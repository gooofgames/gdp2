﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class ButtonManager : MonoBehaviour {



	public void GoToSceneSelector() {
		SceneManager.LoadScene ("LevelSelector");
	}

	public void GoToMainScreen() {
		SceneManager.LoadScene ("MainScreen");
	}

	public void GoToScene1GameSelector() {
		SceneManager.LoadScene ("Scene1GameSelector");
	}
	public void GoToScene2GameSelector() {
		SceneManager.LoadScene ("Scene2GameSelector");
	}
	public void GoToScene3GameSelector() {
		SceneManager.LoadScene ("Scene3GameSelector");
	}
	public void GoToScene4GameSelector() {
		SceneManager.LoadScene ("Scene4GameSelector");
	}
	public void GoToScene5GameSelector() {
		SceneManager.LoadScene ("Scene5GameSelector");
	}
	public void GoToScene6GameSelector() {
		SceneManager.LoadScene ("Scene6GameSelector");
	}
	public void GoToMarketScreen() {
		SceneManager.LoadScene ("MarketScreen");
	}
	public void GoToEditScene(){
		SceneManager.LoadScene ("Datasheet");
	}
	public void GoToGame1_1() {
		//DataManager dataManager = new DataManager ();
		int gameLevel=ApplicationData.levelToStart;
		Debug.Log (gameLevel);
		if (gameLevel == 11) {
			SceneManager.LoadScene ("HowTo");
		} else {
			SceneManager.LoadScene ("Game1_1");
		}
	}
	public void QuitGame(){
		Application.Quit();
	}


	public void PauseAndPlay(){
		if (Time.timeScale == 1) {
			Time.timeScale = 0;
		} else {
			Time.timeScale = 1;
		}
}
}
/*BUG a merket screenen, ha már vásárolt, akkor a back button hatására elveszíti az elköltött gyémántjait, erre egy warning 
ablaknak fel kéne jönnie*/




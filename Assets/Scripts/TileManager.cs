﻿using UnityEngine;
using System.Collections;

public class TileManager : MonoBehaviour {
	public GameObject touchedObject;
	public Vector2 tilePosition;
	public float xmin;
	public float xmax;
	public float ymin;
	public float ymax;
	public GameObject childObject;
	public bool isFree;
	public bool isVertical;


	// Use this for initialization
	void Start () {
	/*
	 Feltöltjük egy tömbbe a tile taggel rendelkező objecteket
	 */
		isFree = true;
		var tileRenderer = this.GetComponent<Renderer> ();
		float width = tileRenderer.bounds.size.x;//tile szélessége
		float height = tileRenderer.bounds.size.y;//tile magassága
		tilePosition=this.transform.position;
		xmin = tilePosition.x-(width/2);//bal oldala
		xmax = tilePosition.x+(width/2);//jobb oldala
		ymin = tilePosition.y-(height/2); //alja
		ymax = tilePosition.y+(height/2); //teteje
		childObject=this.gameObject.transform.GetChild(0).gameObject;	
		//Debug.Log ("Name of childObject:" + childObject.name);
		//Debug.Log ("width:"+ width + "height"+height +"xmin: " + xmin + "xmax: " + xmax + "ymin: " + ymin + "ymax: " + ymax);

	}
	
	// Update is called once per frame
	void Update () {
	/*
	 Megnézzük, hogy áll-e a tile fölött még le nem rakott játékos object 
	 és ha igen, akkor a child objectet aktívra állítjuk
	 */
		touchedObject = TouchManager.pointer; //a le nem rakott player object
		if (touchedObject!=null) {
			if (isFree == true) {
				//Debug.Log ("Touched Object is detected!" + touchedObject.name);
				Vector2 touchPosition = touchedObject.transform.position;
				if (touchPosition.x >= xmin && touchPosition.x <= xmax && touchPosition.y >= ymin && touchPosition.y <= ymax) {
					childObject.SetActive (true);
		
				} else {
					childObject.SetActive (false);
				}
			}

		}
	}
}

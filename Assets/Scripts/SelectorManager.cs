﻿using UnityEngine;
//using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine.UI;

public class SelectorManager : MonoBehaviour {

	public GameObject textField;
	public InputField inputField1;
	public InputField inputField2;
	public InputField inputField3;
	static public List<GameObject> levels = new List<GameObject>();
	static public int listIndex;
	public int listIndexMaximum;
	public GameObject actualObject;
	public Text nameOfResource;

	// Use this for initialization
	void Start () {
		setArray ();
		listIndex = 0;
		nameOfResource = textField.GetComponent<Text> ();
		nameOfResource.text = "" + levels [listIndex].name;
		inputField1.text = levels [listIndex].GetComponent<GameLevelAttributes> ().firstWave;
		inputField2.text = levels [listIndex].GetComponent<GameLevelAttributes> ().secondWave;
		inputField3.text = levels [listIndex].GetComponent<GameLevelAttributes> ().thirdWave;
	

	}
	
	// Update is called once per frame
	void Update () {
	}
	private void setArray(){
		for (int i=11 ; i<70;i++)
		{
			
			string levelName = "GameLevel" + i;
			var obj =Resources.Load(levelName) as GameObject;
			if (obj == null) {
			
			} else {
				levels.Add (obj);
				Debug.Log (obj.name);
			}

	}
		listIndexMaximum = levels.Count-1;

}
	public void UpButtonClick (){
		if (listIndex < listIndexMaximum) {
			listIndex++;
			Debug.Log(levels[listIndex].name);
			nameOfResource.text = "" + levels [listIndex].name;
			inputField1.text = levels [listIndex].GetComponent<GameLevelAttributes> ().firstWave;
			inputField2.text = levels [listIndex].GetComponent<GameLevelAttributes> ().secondWave;
			inputField3.text = levels [listIndex].GetComponent<GameLevelAttributes> ().thirdWave;

		} 
	}

	public void DownButtonClick (){
		if (listIndex > 0) {
			listIndex--;
			Debug.Log (levels [listIndex]);
			nameOfResource.text = "" + levels [listIndex].name;
			inputField1.text = levels [listIndex].GetComponent<GameLevelAttributes> ().firstWave;
			inputField2.text = levels [listIndex].GetComponent<GameLevelAttributes> ().secondWave;
			inputField3.text = levels [listIndex].GetComponent<GameLevelAttributes> ().thirdWave;

		}
	}

	public void updateLevel (){
		var targetPrefab = Resources.Load (levels [listIndex].name);

	//	PrefabUtility.ReplacePrefab(levels[listIndex],targetPrefab,ReplacePrefabOptions.ReplaceNameBased);
	}
}

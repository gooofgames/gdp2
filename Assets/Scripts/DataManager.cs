﻿using UnityEngine;
using System.Collections;
using System;

public class DataManager : MonoBehaviour {

	void Start () {
		
		int myMaxLevel = PlayerPrefs.GetInt ("maxLevel");
		//myMaxLevel = 34;
		if (myMaxLevel == 0) {
			ApplicationData.maxLevel = 11; 
			ApplicationData.diamonds = 3;
			ApplicationData.lastDateTime = System.DateTime.Now; // az első indulásnál beállítjuk az időzítőt
			UpdateDateTime(ApplicationData.lastDateTime); // az első indulásnál elmentjük az időt

		} else {
			ApplicationData.maxLevel = myMaxLevel;
			ApplicationData.diamonds = PlayerPrefs.GetInt("Value");
			long temp = Convert.ToInt64(PlayerPrefs.GetString("lastDTstring"));
			ApplicationData.lastDateTime = DateTime.FromBinary (temp);
		}
		ApplicationData.levelToStart = 0;
	}

	public void UpdateDiamonds(int diamonds){
		ApplicationData.diamonds = diamonds;
		PlayerPrefs.SetInt ("Value",diamonds);
	}

	public void UpdateDateTime (DateTime newDateTime) { //lementi a megadott időt, innen számolódik a két óra
		PlayerPrefs.SetString ("lastDTstring", newDateTime.ToBinary ().ToString ());
		ApplicationData.lastDateTime = newDateTime;
	}

}

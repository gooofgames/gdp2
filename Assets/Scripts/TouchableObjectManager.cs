﻿using UnityEngine;
using System.Collections;

public class TouchableObjectManager : MonoBehaviour {

	public int price;
	public int coins;
	public SpriteRenderer rend;



	// Use this for initialization
	void Start () {
		
		price = this.GetComponent<Raycast> ().price;
		rend = this.GetComponent<SpriteRenderer> ();
	}
	
	// Update is called once per frame
	void Update () {
		coins = GameManager.coins;
		if (coins < price) {
			rend.color = new Color(1f,1f,1f,0.5f);

			//Debug.Log ("halovány");
		} else {
			rend.color = new Color(1f,1f,1f,1f);

			//Debug.Log ("harsány");
		}
	}
}

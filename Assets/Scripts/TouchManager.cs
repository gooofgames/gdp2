﻿using UnityEngine;
using System.Collections;

public class TouchManager : MonoBehaviour {

	public GameObject[] tiles;
	public GameObject[] players;
	public static GameObject pointer;
	public GameObject instantiatedObject;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

		if (Input.touchCount > 0) { //csak akkor fut, ha van touch input
			Touch myTouch = Input.touches[0]; // az első ujj - egyujjas érintést kezelünk
			if (myTouch.phase == TouchPhase.Began) {//kezdő fázis - amikor megérintjük a képernyőt
				GameObject touchArea = GameObject.FindGameObjectWithTag ("TouchArea");//felvesszük a touch area gameobjectet
				var touchAreaRenderer = touchArea.GetComponent<Renderer> (); //touch area renderer komponense
				float taHeight = touchAreaRenderer.bounds.size.y; //touch area magassága
				Vector2 screenPosition = Camera.main.ScreenToWorldPoint (myTouch.position);
				if (screenPosition.y < touchArea.transform.position.y + (taHeight / 2)) { //ha a screenposition a toucharea felső éle alatt van...
				Debug.Log ("Touch begin");
				players = GameObject.FindGameObjectsWithTag ("SelectablePlayer"); //feltöltjük az összes SelectablePlayer taggal rendelkező gameobjectet a players tömbbe
				foreach (GameObject player in players) {
					//Debug.Log ("Player name: " + player.name);
					Vector3 playerPosition = player.transform.position; //felvesszük a player pozicióját
					Vector3 touchPosition = myTouch.position; //felvesszük a touch pozícióját
					var playerRenderer = player.GetComponent<Renderer> ();
					float width = playerRenderer.bounds.size.x; // player szélessége
					float height = playerRenderer.bounds.size.y; // player magassága
					float xmin = playerPosition.x - (width / 2);//bal oldala
					float xmax = playerPosition.x + (width / 2);//jobb oldala
					float ymin = playerPosition.y - (height / 2); //alja
					float ymax = playerPosition.y + (height / 2); //teteje
					//	Vector2 screenPosition = Camera.main.ScreenToWorldPoint(myTouch.position);
					if (screenPosition.x >= xmin && screenPosition.x <= xmax && screenPosition.y >= ymin && screenPosition.y <= ymax) {
						int playerPrice = player.GetComponent<Raycast> ().price;
						int coins = GameManager.coins;
						if (coins >= playerPrice) {
							// a touch az adott object területén belülre esik
							string touchedObjectName = player.name;
							touchedObjectName = touchedObjectName.Substring (0, 6);
							//Debug.Log("Touched object name: " + touchedObjectName);
							instantiatedObject = Instantiate (Resources.Load (touchedObjectName), playerPosition, Quaternion.identity) as GameObject;
							pointer = instantiatedObject;
							pointer.GetComponent<TouchableObjectManager> ().enabled = false;
						}

					} 
				}
			}
			}
		// itt folytatjuk - ez az általános update rész 
			Vector2 screenPos = Camera.main.ScreenToWorldPoint(myTouch.position);
			float screenPosX = screenPos.x;
			float screenPosY = screenPos.y + 0.5f;
			Vector2 modifiedScreenPos = new Vector2 (screenPosX, screenPosY); // módosítjuk, hogy az ujj felett legyen a mozgatott objektum
			pointer.transform.position = modifiedScreenPos;
			if (myTouch.phase == TouchPhase.Ended) {
				bool endedInRightPlace = false;
				tiles = GameObject.FindGameObjectsWithTag ("Tile");
				foreach (GameObject tile in tiles) {
					GameObject childObject = tile.gameObject.transform.GetChild (0).gameObject; // a childobject megtalálása
					if (childObject.activeSelf==true){
						Debug.Log (tile.name + " " + Time.time);
						pointer.transform.position = tile.transform.position; // a mozgatott objektumot a tile közepére rakjuk

						//a horizontális-vertikális tüzelési irány beállítása
						var raycastScript = pointer.GetComponent<Raycast> ();
						var tileManagerScript = tile.GetComponent<TileManager> ();
						if (tileManagerScript.isVertical==true) {
							raycastScript.isVertical = true;
						} else {
							raycastScript.isVertical = false;
						}

						int playerPrice = pointer.GetComponent<Raycast> ().price;
						int coins = GameManager.coins;
						Debug.Log (coins);
						Debug.Log (playerPrice);
						GameManager.coins = coins - playerPrice;
						Debug.Log (GameManager.coins);
						var rayComponent = pointer.GetComponent<Raycast> ();
						rayComponent.setRayPosition ();
						endedInRightPlace=true;
						var tileTM= tile.GetComponent<TileManager> ();
						tileTM.isFree = false;
						childObject.SetActive (false);
					}
				}
				if (endedInRightPlace == false) {
					Destroy (pointer);
				}
				pointer= null;
			}
		}
	
	}
}

﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {
	//Itt vezéreljük a teljes játékot, azaz indítjuk az enemy-ket, meghatározzuk a pályát, a mozgásuk irányát és a sebességüket
	//ezeknek a paramétereit a level prefabból olvassuk ki.
	//
	//INICIALIZÁLÁS:
	//1. meghatározzuk a betöltendő pálya szintjét
	//   a. ha a felhasználó választ pályát, akkor az ahhoz megfelelő prefabot töltjük be
	//   b. a soron következő pályához playerpref-ből kiolvassuk annak értékét és azt töltjük be
	//2. beállítjuk a coinokat
	//3. beállítjuk a pregame screenen beállított(megvett) monstereket
	//4. beállítjuk (feltöltjük tömbbe) a waypointokat
	//5. beállítjuk (feltöltjük tömbbe) a wave-ek definícióját (enemy típus, sebesség, várakozási idő)

	private int gameScene; // betöltendő scene szintje
	private int gameLevel; //betöltendő game szintje
	private GameObject waypointStart;
	private GameObject gameDefinition;
	private GameLevelAttributes levelAttributes;
	public GameObject enemyA;
	public GameObject enemyB;
	public GameObject enemyC;
	public GameObject enemyD;
	public GameObject enemyE;
	public GameObject enemyF;
	public GameObject enemyG;
	public static int score;
	public static int life;
	public GameObject lifeText;
	private Text lfText;
	public GameObject scoreText;
	private Text scText;
	public static int coins;
	public GameObject coinsText;
	private Text coText;
	public GameObject levelText;
	private Text lvlText;
	public GameObject numberOfEnemiesObj;
	private Text numberOfEnemiesText;
	private bool isFinished;
	public GameObject endGamePanel;
	public GameObject endGamePanelTextBox;
	private Text endGamePanelText;
	public DataManager dataManager;
	public Button nextLevel;
	private int numberOfEnemies;
	private int leftoverEnemies;
	private int result;




	// Use this for initialization
	void Start () {
		dataManager = new DataManager();
		Time.timeScale = 1;
		life = 3;
		endGamePanel.SetActive (false);
		Screen.sleepTimeout = SleepTimeout.NeverSleep;


		//Scene betöltése


		/*gameScene = PlayerPrefs.GetInt ("Scene")+1;
		Debug.Log ("Level: " + gameScene);
		string gameSceneName = "GameScene" + gameScene;
		Debug.Log ("Scene name: " + gameSceneName);
		Instantiate (Resources.Load(gameSceneName));*/

		//Level attributes betöltése
		gameLevel=ApplicationData.levelToStart;

		gameScene = gameLevel / 10;
		string gameSceneName = "GameScene" + gameScene;
		/*if (SystemInfo.deviceModel.Contains ("iPad")) {
			Debug.Log ("iPad");
			gameSceneName = gameSceneName + "iPad";
		}*/

		Instantiate (Resources.Load(gameSceneName));

		/*
		if (gameLevel == 1 || gameLevel == 0) {
			gameLevel = 11;
		} else {
		// mi van ha a a gamelevel nagyobb mint egy
		}
*/

		string gameLevelName = "GameLevel" + gameLevel;
		Debug.Log ("Level name: " + gameLevelName);
		Instantiate(Resources.Load(gameLevelName));

		//Cointok betöltése
		//coins=300;

		//waypointok feltöltése ugyanaz, mint az enemy scriptben TODO!!!
		//waypoints=GameObject.FindGameObjectsWithTag("Waypoint");
		waypointStart = GameObject.Find ("Waypoint1");

		/*foreach (GameObject waypoint in waypoints) {
			Debug.Log (waypoint.name + waypoint.transform.position);
		}
		Debug.Log (waypoints.Length);*/

		//game definition feltöltése
		gameDefinition=GameObject.FindGameObjectWithTag("LevelAttributes"); //megkeressük a level attributumokat tartalmazo gameobjectet
		levelAttributes=gameDefinition.GetComponent<GameLevelAttributes>(); // a megtalált gamobject-ből beolvassuk az attributomokat tartalmazó scriptet
		string firstWaveDefinition = levelAttributes.firstWave;
		string secondWaveDefinition = levelAttributes.secondWave;
		string thirdWaveDefinition = levelAttributes.thirdWave;
		coins = levelAttributes.initCoins;
		numberOfEnemies = (firstWaveDefinition.Length + secondWaveDefinition.Length + thirdWaveDefinition.Length)/3;
		leftoverEnemies = numberOfEnemies;
		/*Debug.Log ("First Wave Definition: "+firstWaveDefinition);
		Debug.Log ("Second Wave Definition: "+secondWaveDefinition);
		Debug.Log ("Third Wave Definition: "+thirdWaveDefinition);*/

		score = 0;
		scText = scoreText.GetComponent <Text>();
		coText=coinsText.GetComponent <Text>();
		lvlText = levelText.GetComponent <Text> ();
		lvlText.text = ""+gameLevel;
		lfText = lifeText.GetComponent <Text> ();
		lfText.text = ""+life;
		endGamePanelText = endGamePanelTextBox.GetComponent <Text> ();
		numberOfEnemiesText = numberOfEnemiesObj.GetComponent<Text> ();

		isFinished = false;

		//megvásárolt slotok feltöltése a touch area-ba


		int numberOfSlots = 0;
		string slot1ValueName = PlayerPrefs.GetString("Slot1");
		if (slot1ValueName != "") {
			numberOfSlots ++;
		}
		string slot2ValueName = PlayerPrefs.GetString("Slot2");
		if (slot2ValueName != "") {
			numberOfSlots ++;
		}
		string slot3ValueName = PlayerPrefs.GetString("Slot3");
		if (slot3ValueName != "") {
			numberOfSlots ++;
		}
		string slot4ValueName = PlayerPrefs.GetString("Slot4");
		if (slot4ValueName != "") {
			numberOfSlots ++;
		}
		string slot5ValueName = PlayerPrefs.GetString("Slot5");
		if (slot5ValueName != "") {
			numberOfSlots ++;
		}


		Vector3 slotPoint;
		slotPoint = Camera.main.ScreenToWorldPoint (new Vector3(Screen.width, Screen.height ,0));
		float screenWidthInWorldUnit = slotPoint.x;
		float screenHeightInWorldUnit = slotPoint.y;
		Vector2 putHere;
		Vector3 putHerePrice;
		switch (numberOfSlots) {
		case 1:
			putHere = new Vector3 (0, -(screenHeightInWorldUnit / 7 * 6), 0);
			var newFruit = Instantiate (Resources.Load (slot1ValueName), putHere, Quaternion.identity) as GameObject;
			int fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			var newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;
			Debug.Log ("Price of the only one fruit:" + fruitPrice);
				break;
		case 2:
			putHere = new Vector3 (-(screenWidthInWorldUnit/3), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot1ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;
			putHere = new Vector3 ((screenWidthInWorldUnit/3), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot2ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;

			break;
		case 3:
			putHere = new Vector3 (-(screenWidthInWorldUnit/2), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot1ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;
			putHere = new Vector3 (0, -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot2ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;
			putHere = new Vector3 ((screenWidthInWorldUnit/2), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot3ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;
			break;
		case 4:
			putHere = new Vector3 (-(screenWidthInWorldUnit/5*3), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot1ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;

			putHere = new Vector3 (-(screenWidthInWorldUnit/5), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot2ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;

			putHere = new Vector3 ((screenWidthInWorldUnit/5*3), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot3ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;

			putHere = new Vector3 ((screenWidthInWorldUnit/5), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot4ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;


			break;
		case 5:
			putHere = new Vector3 (-(screenWidthInWorldUnit/3*2), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot1ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;

			putHere = new Vector3 (-(screenWidthInWorldUnit/3), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot2ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;

			putHere = new Vector3 (0, -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot3ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;

			putHere = new Vector3 ((screenWidthInWorldUnit/3*2), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot4ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;

			putHere = new Vector3 ((screenWidthInWorldUnit/3), -(screenHeightInWorldUnit / 7 * 6),0);
			newFruit = Instantiate (Resources.Load (slot5ValueName), putHere, Quaternion.identity) as GameObject;
			fruitPrice = newFruit.GetComponent<Raycast> ().price;
			putHerePrice = new Vector3 (putHere.x, (putHere.y - 0.3f), - 1);
			newPrice = Instantiate (Resources.Load ("PriceText"), putHerePrice, Quaternion.identity) as GameObject;
			newPrice.GetComponent<TextMesh> ().text = "" + fruitPrice;

			break;

			

		}



		//First Wave
		//yield return new WaitForSeconds(10);
		StartCoroutine(EnemyStart(firstWaveDefinition,secondWaveDefinition,thirdWaveDefinition));

	}
	
	// Update is called once per frame
	void Update () {
		scText.text = ""+score;
		coText.text = ""+coins;
		lfText.text = ""+life;
		numberOfEnemiesText.text = "" + leftoverEnemies + "/" + numberOfEnemies;
		if (isFinished == true) { //ha lement az összes enemy
			var isThereAnyObject = GameObject.FindGameObjectWithTag("Monster");
			if (isThereAnyObject==null) {
				StartCoroutine (EndGame ());
			}
		}
		if (life < 1) {
			isFinished = false;
			StartCoroutine (EndGame ());
		}



	}



	IEnumerator EnemyStart (string firstWave,string secondWave,string thirdWave){

		//Feltöltjük tömbbe az enemy definiciót
		int fWDlength = firstWave.Length;
		int sWDlength = secondWave.Length;
		int tWDlength = thirdWave.Length;
		int sumOfEnemies = fWDlength + sWDlength + tWDlength;
		Vector3 startPoint = waypointStart.transform.position;

		//Dictionary definíció
		Dictionary<string,GameObject> enemyList = new Dictionary<string,GameObject>();

		enemyList.Add ("A", enemyA);
		enemyList.Add ("B", enemyB);
		enemyList.Add ("C", enemyC);
		enemyList.Add ("D", enemyD);
		enemyList.Add ("E", enemyE);
		enemyList.Add ("F", enemyF);
		enemyList.Add ("G", enemyG);

		yield return new WaitForSeconds (1.0f);

		//First Wave
		for (int i = 0;i <fWDlength; i++) {
			//Debug.Log (i);

			//soron következő enemy típus, sebesség és szünet feltöltése
			string enemyType = firstWave.Substring (i, 1);
			i++;
			string enemySpeed = firstWave.Substring (i, 1);
			int enemySpeedValue = int.Parse (enemySpeed);// az enemy speedje int-re konvertálva (érték)
			i++;
			string enemyTime = firstWave.Substring (i, 1);
			float enemyTimeValue = float.Parse (enemyTime);
			Instantiate (enemyList [enemyType], startPoint, Quaternion.identity);
			--leftoverEnemies;

			yield return new WaitForSeconds (enemyTimeValue);//szünet másodpercekben
		}
		//between first and second wave
		yield return new WaitForSeconds (5);
		score = score + 100;

		//Second Wave
		for (int i = 0;i <sWDlength; i++) {
			//Debug.Log (i);

			//soron következő enemy típus, sebesség és szünet feltöltése
			string enemyType = secondWave.Substring (i, 1);
			i++;
			string enemySpeed = secondWave.Substring (i, 1);
			int enemySpeedValue = int.Parse (enemySpeed);// az enemy speedje int-re konvertálva (érték)
			i++;
			string enemyTime = secondWave.Substring (i, 1);
			float enemyTimeValue = float.Parse (enemyTime);
			Instantiate (enemyList [enemyType], startPoint, Quaternion.identity);
			--leftoverEnemies;



			yield return new WaitForSeconds (enemyTimeValue);//szünet másodpercekben
		}
		//between second and third wave
		yield return new WaitForSeconds (5);
		score = score + 200;

		//Third wave
		for (int i = 0;i <tWDlength; i++) {
			//Debug.Log (i);

			//soron következő enemy típus, sebesség és szünet feltöltése
			string enemyType = thirdWave.Substring (i, 1);
			i++;
			string enemySpeed = thirdWave.Substring (i, 1);
			int enemySpeedValue = int.Parse (enemySpeed);// az enemy speedje int-re konvertálva (érték)
			i++;
			string enemyTime = thirdWave.Substring (i, 1);
			float enemyTimeValue = float.Parse (enemyTime);
			Instantiate (enemyList [enemyType], startPoint, Quaternion.identity);
			--leftoverEnemies;



			yield return new WaitForSeconds (enemyTimeValue);//szünet másodpercekben
		}

		isFinished = true;
		/* TODO
		 * csak akkor állítódjon az isFinihed true-ra ha az utolsó enemy is eltűnt (destroyed)
		 * ezt valahogy úgy kéne, hogy
		 * 1. megszámoljuk az összes enemy-t
		 * 2. minden destroy-nál számolunk egyet
		 * 3. és ha a két szám egyezik, akkor állítjuk az isFinished-et true-ra
		 * 
		*/

		/*megvárjuk amíg az összes enemy eltűnik a színről!!!
		var isThereAnyObject = GameObject.FindGameObjectWithTag("Monster");
		while (isThereAnyObject) { 
			yield return new WaitForSeconds(0.1f);
		}
		Debug.Log ("Vége");
		yield return new WaitForSeconds (2f);
		score = score + 300;
		SceneManager.LoadScene ("Scene1GameSelector");
	*/

	}
	IEnumerator EndGame(){
		yield return new WaitForSeconds (1.5f);
		Time.timeScale = 0;
		/* 
		 * Itt fel kéne jönni egy kontrollnak, ahol ha a játék failed, akkor felkínálja
		 * ujrajátszást és a kilépést,
		 * ha sikeres, akkor az ujrajátszást kilépést és a következő szintre lépést
		 *
		 */
		endGamePanel.SetActive (true);
		if (isFinished) {

			SetLevel ();
			endGamePanelText.text = gameLevel%10==9? "Hey, you won and unlocked the next Scene! Your benefits are " + life + " gems!":"You won! Your score is " + score +" and you have got "+ life +" gems!";
			//TODO itt	 meg lehetne csinálni, hogy annyi gyémántot kapjon, ahány élete maradt (1-3)
			dataManager.UpdateDiamonds (ApplicationData.diamonds+life);	
		} else {
			endGamePanelText.text = "You failed! Try again or quit level!";
			nextLevel.interactable = false;
		}
		Screen.sleepTimeout = SleepTimeout.SystemSetting;
		//SceneManager.LoadScene ("Scene1GameSelector");
		if (isFinished == true && gameLevel == ApplicationData.maxLevel){
			int increaser = ApplicationData.maxLevel%10==9? 2:1; //ha 9-es a vége akkor kettővel növeljük a maxlevelt, egyébként eggyel
			ApplicationData.maxLevel=ApplicationData.maxLevel+increaser;
			Debug.Log ("Gamemanager maxlevel:"+ ApplicationData.maxLevel);
			PlayerPrefs.SetInt ("maxLevel", ApplicationData.maxLevel);
		}
	}

	public void EndGamePanelQuitButton(){
		SceneManager.LoadScene ("LevelSelector");
	}

	public void EndGamePanelRestartButton(){
		ApplicationData.levelToStart = gameLevel;
		SceneManager.LoadScene ("MarketScreen");
	}

	public void EndGamePanelNextLevelButton(){
		if (isFinished) {
			ApplicationData.levelToStart = gameLevel % 10 == 9 ? gameLevel + 2 : gameLevel + 1; //TODO:  kezelni kell, ha az eredmény 20, 30 stb!!!
			SceneManager.LoadScene ("MarketScreen");
		} 
	}
	public void SetLevel(){
		
		string levelName = "Level" + gameLevel;
		int storedResult = PlayerPrefs.GetInt (levelName);
		if (storedResult < life) {
			switch (life) {
			case 3:
				result = 3;
				break;
			case 2:
				result = 2;
				break;
			case 1:
				result = 1;
				break;
			}
			PlayerPrefs.SetInt (levelName,result);

		}
		int increaser = 1;
		if (gameLevel == ApplicationData.maxLevel) {
			if (gameLevel % 10 == 9) {
				increaser=2;
			}
			levelName = "Level" + (gameLevel + increaser);
			Debug.Log ("playerprefbe mentve: " +levelName);

			PlayerPrefs.SetInt (levelName,1);
		}
	}
}

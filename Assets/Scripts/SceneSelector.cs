﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SceneSelector : MonoBehaviour {
	public GameObject[] scenes;
	public int activeScene;
	public Text sceneText;

	// Use this for initialization
	void Start () {
		int maxLevel = ApplicationData.maxLevel;
		int maxScene = (maxLevel / 10) - 1;
		activeScene = maxScene;
		AllSetInactive();	
		scenes [activeScene].SetActive (true);
		sceneText.text = scenes [activeScene].name;
	}
	
	// Update is called once per frame
	void Update () {
		
	}
	public void PlusButton (){
		if (activeScene < 5) 
		{
			activeScene++;
			AllSetInactive();
			scenes [activeScene].SetActive (true);
			sceneText.text = scenes [activeScene].name;

		}
	}

	public void MinusButton (){
		if (activeScene > 0) 
		{
			activeScene--;
			AllSetInactive();
			scenes [activeScene].SetActive (true);
			sceneText.text = scenes [activeScene].name;

		}
	}

	public void AllSetInactive (){
		for (int i = 0; i < 6; i++) 
		{
			scenes [i].SetActive (false);
		}
	}
}

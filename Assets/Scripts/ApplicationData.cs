﻿	public class ApplicationData
	{
	static public int levelToStart;
	static public int maxLevel;
	static public int coins;
	static public int diamonds;
	static public System.DateTime lastDateTime;
	}

/*
 * Level kezelés: a pályákat kétjegyű int-ben tároljuk. 
 * Az első jegy a scene-t jelöli, a második a hozzá tartozó 
 * level-t. Ebből következik, hogy egy scene-hez 9 level 
 * tartozhat (a nullát nem használjuk). Amikor egy level-t indítunk
 * (bárhonnan, akkor a fenti levelToStart értéket kell beállítani,
 * ezt fogja kiolvasni és feldolgozni a GameManger. 
 * Amikor egy Levelt sikeresen befejezünk, akkor - ha szükséges- 
 * be kell állítani a maxLevel értéket a fenti változóban és
 * PlayerPrefs.setInt-ben egyaránt!!!
*/

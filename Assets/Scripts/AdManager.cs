﻿using UnityEngine;
using System.Collections;
using UnityEngine.Advertisements;
using UnityEngine.UI;


public class AdManager : MonoBehaviour {
	public static int diamonds;
	public Text diamondsText;
	public Font myFont;
	public DataManager dataManager;
	public Button adButton;
	public Text adButtonText;
	//public System.DateTime lastDateTime; 
	//public System.DateTime currentDateTime;
	public int maxDiamonds;
	//public Text debugText;


	// Use this for initialization
	void Start () {
		dataManager= new DataManager();

		//TODO a slot állapotokat a setstring helyett a DataManagerrel kéne megoldani
		PlayerPrefs.SetString ("Slot1", "null");
		PlayerPrefs.SetString ("Slot2", "null");
		PlayerPrefs.SetString ("Slot3", "null");
		PlayerPrefs.SetString ("Slot4", "null");
		PlayerPrefs.SetString ("Slot5", "null");
		Debug.Log ("Level To Start: "+ApplicationData.levelToStart);
	}
	
	// Update is called once per frame
	void Update () {
		diamondsText.text = "" + ApplicationData.diamonds;
	}
		
	void OnGUI() {
		if (Advertisement.IsReady ()) {
			adButtonText.text = "See Ad and get more gems!";
			adButton.interactable = true;
		} else {
			adButtonText.text = "Waiting...";
			adButton.interactable = false;
		}

		}

	public void PressAdButton(){
		Advertisement.Show (null, new ShowOptions {
			resultCallback = result => {
				switch (result) {
				case (ShowResult.Finished):
					dataManager.UpdateDiamonds (ApplicationData.diamonds + 5);
					break;
				}
			}
		});
	}

}
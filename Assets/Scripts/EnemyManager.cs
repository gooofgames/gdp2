﻿using UnityEngine;
using System.Collections;

public class EnemyManager : MonoBehaviour {
	private Transform _waypoints;
	private int _targetWaypoint = 0;
	public float speed = 0.5f;
	public int fullHealth;
	public int health;
	private int addHealth;
	public Animator anim;
	public bool isOnTheEndOfPath;
	public GameObject brokenHeartParticleObject;
	public GameObject heartParticleObject;
	public float decreaseHealthInSeconds; //hány másodpercenként csökkenjen a health - editorban kell beállítani
	public GameObject healthBarForeGround;
	public float healthBarMaxScale;


	// Use this for initialization
	void Start () {
		_waypoints=GameObject.Find("Waypoints").transform;
		health = fullHealth;
		HealthBarSetting ();
		InvokeRepeating ("decreaseHealth", decreaseHealthInSeconds, decreaseHealthInSeconds);


	}

	void decreaseHealth () {
		health--;
		HealthBarSetting ();

	}
	
	// Update is called once per frame
	void Update () {
		if (health > 0) { //ha elfogyott az élet, ne mozogjon tovább, a death animáció álló helyzetben történjen
			MovingOnPath ();
		}
		if (health == 0) {
			isOnTheEndOfPath=false;
			StartCoroutine(DeathAnimation());

		}
		if (health > fullHealth) { //4-ben maximalizáljuk a health értékét
			health = fullHealth;
			HealthBarSetting ();
		}

	}

	private void MovingOnPath(){
		Transform targetWaypoint = _waypoints.GetChild (_targetWaypoint);
		//Debug.Log (targetWaypoint);
		Vector3 relative = targetWaypoint.position - transform.position;
		float distanceWaypoint = relative.magnitude;
		Vector3 targetPosition = targetWaypoint.position;

		if (distanceWaypoint < 0.1) {
			if (_targetWaypoint + 1 < _waypoints.childCount) {
				_targetWaypoint++;
			} else {
				isOnTheEndOfPath = true;
				StartCoroutine (DeathAnimation ());
				return;
			}
		} else {
			transform.position = Vector3.MoveTowards (transform.position, targetPosition, speed * Time.deltaTime);
		}
	}
	void OnTriggerEnter2D (Collider2D other) { //az ütközés lekezelése
		StartCoroutine (FeedAnimation()); //ez indítja a feed animation-t - egyelőre csak food objecttel üközünk, azért van itt
		string otherGameObjectTag = other.gameObject.tag;
		addHealth = other.GetComponent<Firedobject> ().foodValue;
		health = health+addHealth;
		HealthBarSetting ();
		CancelInvoke ();
		InvokeRepeating ("decreaseHealth", decreaseHealthInSeconds, decreaseHealthInSeconds);
		ParticleSystem myParticle = heartParticleObject.GetComponent<ParticleSystem>();
		myParticle.Play();
	}

	private void HealthBarSetting(){
		float actScale =  ((float) health / (float) fullHealth) * healthBarMaxScale;
		Debug.Log (healthBarMaxScale);
		Debug.Log (actScale);
		Debug.Log (health);
		Debug.Log (health / fullHealth);
		healthBarForeGround.transform.localScale = new Vector3 (actScale, healthBarForeGround.transform.localScale.y,transform.localScale.z);
	}

	IEnumerator FeedAnimation() {
		Debug.Log ("anim");
		anim = GetComponent<Animator> ();
		anim.SetTrigger ("isFeed");
		yield return new WaitForSeconds (0.1f);
	} 
	IEnumerator DeathAnimation(){
		if (isOnTheEndOfPath == true) {
			GameManager.score = GameManager.score + 100;
			GameManager.coins = GameManager.coins + 100;
			Destroy (gameObject);
		} else {
			health = -10;
			if (GameManager.life > 0) {
				GameManager.life--;
			}
			ParticleSystem myParticle = brokenHeartParticleObject.GetComponent<ParticleSystem>();
			myParticle.Play();
			anim = GetComponent<Animator> ();
			anim.SetTrigger ("isDead");
			yield return new WaitForSeconds (1.5f);
			Destroy (gameObject);
		}
	}
}
			
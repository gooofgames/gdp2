﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class LevelSelectorManager : MonoBehaviour {
	private int maxLevelValue;
	public int sceneLevel;
	public GameObject[] buttons;

	// Use this for initialization
	void Start () {

		// először az összes gombot disabled-re állítjuk

		for (int i = 0; i < buttons.Length; i++) {
			var myButton = buttons [i];
			var myButtonScript = myButton.GetComponent<Button>();
			myButtonScript.interactable = false;
			}
			
		maxLevelValue = ApplicationData.maxLevel < (sceneLevel*10)+9 ?  ApplicationData.maxLevel:(sceneLevel*10)+9;
		Debug.Log ("maxLevelValue= " + maxLevelValue);
		//majd beolvassuk a gamelevelt, és az annak megfelelő pályákat enabled-re állítjuk
		int myButtonIndex = 0;
		for (int i = sceneLevel*10; i < maxLevelValue; i++) {
			var myButton = buttons [myButtonIndex];
			var myButtonScript = myButton.GetComponent<Button>();
			myButtonScript.interactable = true;
			myButtonIndex++;

		}
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

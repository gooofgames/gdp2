﻿using UnityEngine;
using System.Collections;

public class GameLevelAttributes : MonoBehaviour {

	//az egyes hullámokat leírását stringben tároljuk három karakterenként, az első karakter egy betű, ami az enemy
	//típusát jelzi, 
	//a második egy szám 1-9ig, ami a sebességet határozza meg,
	//a harmadik egy szám 1-9-ig  - ez egy viszonyszám - amiből a következő enemy várakozási idejét számoljuk

	public string firstWave;
	public string secondWave;
	public string thirdWave;
	public int initCoins; 


}

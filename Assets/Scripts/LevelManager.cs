﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class LevelManager	 : MonoBehaviour {
	public int result; // 0,1,2,3
	public Sprite sprite0;
	public Sprite sprite1;
	public Sprite sprite2;
	public Sprite sprite3;
	private Image myImageComponent; 
	private Button myButtonComponent;
	private Text myTextComponent;
	private int levelToStart;

	// Use this for initialization
	void Awake () {
		levelToStart = int.Parse (this.name.Substring (this.name.Length - 2));
		myImageComponent = GetComponentInChildren <Image> ();
		myButtonComponent = GetComponentInChildren <Button> ();
		myTextComponent = GetComponentInChildren<Text> ();
		string levelNumber = this.name.Substring(this.name.Length -1);
		myTextComponent.text = null;

		if (levelToStart == 11 && ApplicationData.maxLevel<12) {
			myImageComponent.sprite = sprite1;
			myButtonComponent.interactable = true;
			myTextComponent.text = levelNumber;
		} else {
			GetState ();
			Debug.Log (levelToStart);

			switch (result) {
			case 0: 
				myImageComponent.sprite = sprite0;
				myButtonComponent.interactable = false;
				break;
			case 1: 
				myImageComponent.sprite = sprite1;
				myButtonComponent.interactable = true;
				myTextComponent.text = levelNumber;
				break;
			case 2: 
				myImageComponent.sprite = sprite2;
				myButtonComponent.interactable = true;
				myTextComponent.text = levelNumber;
				break;
			case 3: 
				myImageComponent.sprite = sprite3;
				myButtonComponent.interactable = true;
				myTextComponent.text = levelNumber;
				break;
			}
		}
	}

	public void GetState (){
		string myLevelName = this.name;
		result=PlayerPrefs.GetInt (myLevelName);
	}
	public void pressButton(){
		SceneManager.LoadScene ("MarketScreen");
		ApplicationData.levelToStart = levelToStart;
	}


}

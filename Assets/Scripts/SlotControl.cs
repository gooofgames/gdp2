﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SlotControl : MonoBehaviour {
	public Button playButton;
	public GameObject mySlot1;
	 

	// Use this for initialization
	void Start () {

	}
	
	// Update is called once per frame
	void Update () {
		var mySlotState = mySlot1.GetComponentInChildren<SlotManager>().slotState;

		if (mySlotState == "Empty" ) {
			playButton.interactable = false;
		} else {
			playButton.interactable = true;
		}
	}
}

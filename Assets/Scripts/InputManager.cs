﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class InputManager : MonoBehaviour {

	// Use this for initialization
	void Start () {
		var input = gameObject.GetComponent<InputField>();
		input.onEndEdit.AddListener(SubmitName);  // This also works

	}

	private void SubmitName (string arg0){
		Debug.Log (arg0);
		GameObject go = SelectorManager.levels[SelectorManager.listIndex];
		string thisName = this.name;
		string lastChar = thisName.Substring (thisName.Length - 1);
		if (lastChar == "1") {
			go.GetComponent<GameLevelAttributes> ().firstWave = arg0;
		} else if (lastChar == "2") {
			go.GetComponent<GameLevelAttributes> ().secondWave = arg0;
		} else {
			go.GetComponent<GameLevelAttributes> ().thirdWave = arg0;

		}

	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class SceneSelectorManager : MonoBehaviour {
	public int sceneValue;
	public GameObject[] buttons;

	// Use this for initialization
	void Start () {

		// először az összes gombot disabled-re állítjuk

		for (int i = 0; i < buttons.Length; i++) {
			var myButton = buttons [i];
			var myButtonScript = myButton.GetComponent<Button>();
			myButtonScript.interactable = false;
		}
		sceneValue = ApplicationData.maxLevel/10;
		//Debug.Log ("MaxLevel: " + ApplicationData.maxLevel);
		//Debug.Log ("SceneValue: " + sceneValue);

		//majd beolvassuk a gamelevelt, és az annak megfelelő pályákat enabled-re állítjuk
		for (int i = 0; i < sceneValue; i++) {
			var myButton = buttons [i];
			var myButtonScript = myButton.GetComponent<Button>();
			myButtonScript.interactable = true;
		}			
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class MusicManager : MonoBehaviour {
	private static MusicManager instance = null;
	public static MusicManager Instance {
		get { return instance; }
	}


	// Use this for initialization
	void Awake () {

		if (instance != null && instance != this) {
			Destroy (this.gameObject);
			return;
		} else {
			instance = this;
		}

		DontDestroyOnLoad (this.gameObject);
	}
	
	// Update is called once per frame
	void Update () {
	
	}
}

﻿using UnityEngine;
using System.Collections;

public class OnlyForTest : MonoBehaviour {
	public Animator anim;


	// Use this for initialization
	void Start () {
		StartCoroutine (FeedAnimation ());
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	IEnumerator FeedAnimation() {
		Debug.Log ("anim");
		anim = GetComponent<Animator> ();
		anim.SetBool ("isFeed", true);
		yield return new WaitForSeconds (1.5f);
		anim.SetBool ("isFeed", false);
	}
}

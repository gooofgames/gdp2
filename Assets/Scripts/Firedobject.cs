﻿using UnityEngine;
using System.Collections;

public class Firedobject : MonoBehaviour {

	//public GameObject targetObject;
	public Vector2 startPosition;
	public Vector2 targetPosition;
	public float speed=10f;
	public bool isVertical;
	public bool isChainDestroyer; //ha true, akkor nem pusztul el az ütközésnél, hanem megy tovább
	public int foodValue; //mennyivel nő találat esetén az élet
	//public GameObject targetGameObject;

	// Use this for initialization
	void Start () {
		startPosition = transform.position;

		float startX = startPosition.x;
		float startY = startPosition.y;

		if (isVertical == true) {
			float targetY = startY + 20f;
			targetPosition = new Vector2 (startX, targetY);
		} else {
			float targetX = startX - 20f;
			targetPosition = new Vector2 (targetX, startY);
		
		}
		//targetPosition = targetGameObject.transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		//targetPosition = targetGameObject.transform.position;
		transform.position = Vector2.MoveTowards (transform.position, targetPosition, speed * Time.deltaTime);
		if (transform.position.x == targetPosition.x && transform.position.y == targetPosition.y) {
			Destroy (gameObject);
		}
	}

	void OnTriggerEnter2D(Collider2D other){
		//Debug.Log ("Ütközés!");
		if (other.gameObject.tag == "Monster") {
			if (isChainDestroyer == false) {
				Destroy (gameObject);
			}
			GameManager.score = GameManager.score + 10;
		}
	
	}

}

﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class TimeManager : MonoBehaviour {
	public System.DateTime lastDateTime;
	public System.DateTime currentDateTime;
	public DataManager dataManager;
	public int maxDiamonds;
	public Text debugText;
	public Text nextGemText;


	// Use this for initialization
	void Start () {
		InvokeRepeating ("UpdateTime", 0, 5);
		
		//UpdateTime ();
	}
	
	// Update is called once per frame
	void Update () {
	
	}
	public void UpdateTime(){
		dataManager= new DataManager();
		lastDateTime = ApplicationData.lastDateTime;
		currentDateTime=System.DateTime.Now;
		Debug.Log (lastDateTime);
		Debug.Log (currentDateTime);

		System.TimeSpan differenceDateTime = currentDateTime - lastDateTime;
		double differenceInHours = differenceDateTime.TotalHours;
		double differenceInHoursRounded = (differenceInHours/1)-  (differenceInHours % 1);

		if (differenceInHoursRounded>=1) 
		{
			int dif = (int)differenceInHoursRounded;
			//ApplicationData.lastDateTime = currentDateTime.AddHours (2);
			ApplicationData.diamonds= ApplicationData.diamonds+Mathf.Min (dif,maxDiamonds);
			if (dif > maxDiamonds) {
				dataManager.UpdateDateTime (currentDateTime);
			} else {
				dataManager.UpdateDateTime (lastDateTime.AddHours (dif));
			}
		}

		differenceDateTime = currentDateTime - lastDateTime;
		double differenceInMinutes = differenceDateTime.TotalMinutes;
		double nextDiamondinMinutes = 60-(differenceInMinutes % 60);
		int intNextDiamondinMinutes = (int)nextDiamondinMinutes;
		nextGemText.text = "Next: " + intNextDiamondinMinutes + "m";

		string model = SystemInfo.deviceModel;
		if (SystemInfo.deviceModel.Contains("iphone")){
			Debug.Log ("ez egy Iphone!");
		}

		debugText.text = "Last Time: " + lastDateTime
			+ " Current time: " + currentDateTime
			+ " Difference in hours: " + differenceInHoursRounded
			+ " Difference as int: " + (int)differenceInHoursRounded
			+ "Difference in mins: " + differenceInMinutes
			+ " New lastDateTime: " + ApplicationData.lastDateTime
			+ " MathfCalculation: " + Mathf.Min((int)differenceInHoursRounded,maxDiamonds)
			+ " Next diamond: " + intNextDiamondinMinutes
			+ " System: "+model;
		
		
	}

}

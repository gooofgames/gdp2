﻿using UnityEngine;
using System.Collections;

public class Raycast : MonoBehaviour {
	public Vector2 sightStart, sightEnd;
	public bool spotted = false;
	public float fireRate;
	public float lastFire;
	public GameObject fireObject;
	public int price;
	public bool isVertical;
	//public GameObject trgObj;

	// Use this for initialization
	void Start () {

		lastFire = -25f;
		//Debug.Log ("isVertical: " + isVertical);
		sightStart = transform.position;
		sightEnd = transform.position;

	}

	public void setRayPosition() {
		sightStart = transform.position;
		float startX = sightStart.x;
		float startY = sightStart.y;

		if (isVertical == true) {
			float targetY = startY + 20f;
			sightEnd = new Vector2 (startX, targetY);
		} else {
			float targetX = startX - 20f;
			sightEnd = new Vector2 (targetX, startY);
		}
		Debug.Log (this.name + sightStart + " " + sightEnd);		
	}

	// Update is called once per frame
	void Update () {
		Debug.DrawLine (sightStart, sightEnd, Color.blue);
		spotted = Physics2D.Linecast (sightStart, sightEnd, 1 << LayerMask.NameToLayer("Monster"));
		if (spotted == true) {
			if (Time.time >= lastFire + fireRate) {
				Debug.Log ("Spotted at time");
				RaycastHit2D[] obs = Physics2D.LinecastAll (sightStart, sightEnd, 1 << LayerMask.NameToLayer ("Monster"));
				//trgObj = obs [0].collider.gameObject;
				Fire ();
				lastFire = Time.time;
			}
		} 
	}

	public void Fire(){
		//Debug.Log("Fire");
		//megjelenítjük a fire objectet, és beállítjuk a targetet:
		//megnézzük, hány monster object van a raycastban, felmérjük a távolságot
		// és a legközelebbit beállítjuk célnak
		var firedObject = (GameObject) Instantiate(fireObject,transform.position,Quaternion.identity);

		//átadjuk a firedObjecthez rendelt scriptnek az isVertical változó értékét
		//vagyis hogy felfelé vagy balra tüzeljen
		var firedObjectScript = firedObject.GetComponent<Firedobject> ();
		firedObjectScript.isVertical = isVertical;
	}

}
